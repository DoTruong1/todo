import {call, cancel, join, take, put, takeEvery, takeLatest, all, fork, select} from "redux-saga/effects"
import * as action from '../../actions/actions'
// import getTodos from '../selector'
import {Types} from "../../actions/constant"
import axios from "axios";
import env from "react-dotenv";

const BE_API = window._env_.REACT_APP_BE_API

const getTodoList =  async () => {
    console.log('fetch async')
    const uri = BE_API + "/todo/";
    console.log(uri);
   return await axios.get(uri);
}


function* fetchTodoList() {
    try {

        const res = yield call(getTodoList);
        
        console.log("fetch todo")
        console.log(res.data)
        yield put(action.getTodoListSuccess(res.data))
    } catch (e) {
        console.warn(e.message)
    }
}

async function deleteTodoAsync(id) {
    const uri = BE_API + `/todo/${id}`
    await axios.post(uri).then(() => console.log("post done"))
    console.log("post done")
}

async function addTodoAsync(detail) {
    console.warn(detail)
    const uri = BE_API + "/todo/add";
    await axios.post(uri, 
        {
            details: detail
        } ,
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )

} 

function* deleteTodo({id}) {
    try { 
        console.log("delete called ")
        const res = yield call(deleteTodoAsync, id)
        console.log("delete called 2")
        // console.log(res)
        yield put(action.remove(id));
    } catch(e) {
        console.error(e);
    }
}

function* addTodo({detail}) {
    try {
        yield call(addTodoAsync, detail)
        const res = yield call(getTodoList);
        console.log("ADDDDDD")
        console.log(res.data)
        yield put(action.getTodoListSuccess(res.data));
    } catch(e) {
        console.warn(e)
    }
}

export function* watchFetchTodo () {
    yield takeLatest(Types.GETLIST, fetchTodoList);
}

export function* deleteWatcher() {
    yield takeEvery(Types.DELETE_TODO_REQ, deleteTodo);
} 

export function* addWatcher() {
    yield takeEvery(Types.ADD_REQUEST, addTodo)
}
export default function* todoSagas() {
    yield all[watchFetchTodo(), deleteWatcher(), addWatcher()]
}

// export default function* rootSaga() {
//     yield all([
//       watchFetchTodo(),
//         deleteWatcher()
//     ])
//     // code after all-effect
//   }